<?php

ini_set('include_path', __DIR__);

//include_once "App/autoloader.php";
include_once "vendor/autoload.php";

use App\Vehicle\Car;
use App\Exceptions\TooFastException;
use App\Vehicle\Truck;
use App\Api\VehicleInterface;

$vwGolf = new Car(
    "Golf",
    200,
    0
);

try {
    $vwGolf->accelerate(null);
} catch (TooFastException $e) {
    echo "TooFastException cached: ".$e->getMessage()."\n";
} catch (\Exception $exception) {
    echo "Exception cached: ".$exception->getMessage()."\n";
}


$renoClio  = new Car(
    "Reno Clio",
    200,
    0
);


$mercedesTruck = new Truck(
    "Mercedes"
);

if ($mercedesTruck instanceof VehicleInterface) {
    echo "ok";
} else {
    echo "not ok";
}
echo "\n";
