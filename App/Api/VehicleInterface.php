<?php

namespace App\Api;

interface VehicleInterface
{
    const MAX_SPEED = 450;

    public function accelerate( $newSpeed);
}
