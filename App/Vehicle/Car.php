<?php

namespace App\Vehicle;

use App\Vehicle\Car\Wheal as CarWheel;
use App\Vehicle\Truck\Wheal as TruckWheal;

class Car extends VehicleAbstract
{
    static $numberOfWheals = 4;

    public function __construct(
        $model,
        $maxSpeed = 200,
        $currentSpeed = 0
    )
    {
        $this->model=$model;
        $this->currentSpeed = $currentSpeed;
        $this->maxSpeed = $maxSpeed;
        $this->wheals = new CarWheel(15,2);
    }

    static function getNumberOfWheals()
    {
        return self::$numberOfWheals;
    }

    static function setNumberOfWheals($newNumber)
    {
        self::$numberOfWheals = $newNumber;
    }

    public function slowDown($newSpeed)
    {
        while ($this->currentSpeed > $newSpeed)
        {
            $this->setSpeed($this->currentSpeed - 1);
        }
    }
}
