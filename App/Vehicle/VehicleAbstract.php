<?php

namespace App\Vehicle;

use App\Api\VehicleInterface;
use App\Api\SedanInterface;
use App\Exceptions\TooFastException;

abstract class VehicleAbstract implements VehicleInterface, SedanInterface
{
    public $model;

    protected $maxSpeed;

    protected $maxLoad;

    protected $currentSpeed;

    protected $wheals;

    protected $speed;
    protected $length;
    protected $width;


    public function accelerate($newSpeed)
    {
        $logger = new \Katzgrau\KLogger\Logger(__DIR__.'/logs');

        if ($newSpeed > self::MAX_SPEED) {
            $logger->error("Exception logged");
            throw new TooFastException("Too fast");
        }

        if (is_null($newSpeed)) {
            $logger->error("Exception logged");
            throw new \Exception("Speed is empty");
        }

        while ($this->currentSpeed < $newSpeed)
        {
            $this->setSpeed($this->currentSpeed + 1);
        }
    }

    protected function setSpeed($newSpeed)
    {
        $this->currentSpeed = $newSpeed;
    }

    public function getSpeed()
    {
        echo $this->currentSpeed;
        echo "\n";
    }

    public abstract function slowDown($newSpeed);

    public function __call($name, $arguments)
    {
        $method = substr($name,0,3);
        $variable = strtolower(substr($name,3));

        if ($method == "set") {
            $this->$variable = $arguments[0];
            return $this;
        }
        if ($method == "get") {
            return $this->$variable;
        }

//        $this->$name($arguments[0]);
    }

    public function __set($name, $value)
    {
        $this->$name = $value;
    }
}
