<?php

namespace App\Vehicle;

class Truck extends Car
{
    private $currentLoad;

    public function __construct(
        $model,
        $maxSpeed = 200,
        $currentSpeed = 0,
        $maxLoad = 35000,
        $currentLoad = 0
    )
    {
        $this->maxLoad = $maxLoad;
        $this->currentLoad = $currentLoad;
        $this->currentSpeed = $currentSpeed;
        parent::__construct($model, $maxSpeed);
    }
}
